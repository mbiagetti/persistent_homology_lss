import numpy as np
import multiprocessing as mp,os

def loadfile(fname):
    '''only works for well-formed text file of space-separated doubles'''

    rows = []  # unknown number of lines, so use list
    with open(fname) as f:
        for line in f:
            line = [float(s) for s in line.split()[:4]]    #select only Mass and Pos(3)
            rows.append(np.array(line, dtype = np.double))
    return rows  # convert list of vectors to array

def savePD(d,filename):
    toSave=[]
    for elm in d:
        if elm[1][1]!=elm[1][0]:
            toSave.append([elm[0],elm[1][0],elm[1][1]])
    np.savetxt(filename,toSave,delimiter=',')
    
def cleanPD(pd,cut,power=0.5):
    '''change PD from coordinates (birth,death) to (birth, persistence),
        with option to rescale filtration parameter by power'''
    p0=[]
    p1=[]
    p2=[]
    for elm in pd:
        if elm[0]==2 and elm[1][1]!=elm[1][0] and elm[1][1]>(float(cut)/2)**(power/2):
            p2.append([np.power(elm[1][0],power),np.power(elm[1][1]-elm[1][0],power)])
        elif elm[0]==1 and elm[1][1]!=elm[1][0] and elm[1][1]>(float(cut)/2)**(power/2):
            p1.append([np.power(elm[1][0],power),np.power(elm[1][1]-elm[1][0],power)])
        elif (np.isinf(elm[1][1]))==False and elm[1][1]!=elm[1][0] and elm[1][1]>(float(cut)/2)**(power/2):
            p0.append([np.power(elm[1][0],power),np.power(elm[1][1]-elm[1][0],power)])
    return np.array(p0),np.array(p1),np.array(p2)
