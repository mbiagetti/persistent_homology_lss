#!/bin/sh

cd ../

#export PYTHONPATH=$PYTHONPATH:/home/matteob/test_installation/gudhi.3.0.0/build/python
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/Codes/Libraries

hdir=/Users/acole/repos/persistent_homology_lss/Code/TestData/
hsim=gaussian_1536_2Gpc_085_run1
hdir2=/FOF/Rockstar
#hdir=/projects/0/matteob/Nbody/byagox_gaussian_1536_2Gpc_085
hcat=out_z2_nosub_M200b_20p.list
bounds=Utils/genBounds/Output/z2-NoSubR_gaussian_1536_2Gpc_085_run1.txt
pdfile=Output/Subsampling/PD/z2-NoSubR_alphaDTM_
EXT=.dat

start=$(date)
echo "start time : $start"

python main.py  --hfile ${hdir}${hcat}  --pi True  --boundsName ${bounds}  --pdfile ${hdir}${pdfile}${hsim}${EXT} --cut 0 --onedim True
