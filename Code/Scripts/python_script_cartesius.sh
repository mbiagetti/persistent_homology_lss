#!/bin/sh

# THE NAME OF THE SBATCH JOB

#SBATCH -J ph

# REDIRECT OUTPUT TO THE FOLLOWING STREAM
#SBATCH -e ../RUNinfo/ph-error%A_%a
#SBATCH -o ../RUNinfo/ph-out%A_%a

# NUMBER OF NODES
#SBATCH -n 1
#SBATCH --array 1
#SBATCH --mincpus=24

# ENVIRONMENT

#SBATCH --partition=short
##SBATCH --constraint=[island1|island2|island3|island4|island5] 

# SET THE TIME LIMIT [HRS:MINS:SECS]
#SBATCH --time=01:00:00

##SBATCH --mail-user=acole4@wisc.edu
#SBATCH --mail-user=m.biagetti@uva.nl
#SBATCH --mail-type=ALL


#module load 2019
#module load Python/3.6.6-intel-2018b

cd ../

#export PYTHONPATH=$PYTHONPATH:/home/matteob/test_installation/gudhi.3.0.0/build/python
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/Codes/Libraries

hdir=/projects/0/matteob/Nbody/byagox_
hsim=gaussian_1536_2Gpc_085_run1
hdir2=/FOF/Rockstar/
#hdir=/projects/0/matteob/Nbody/byagox_gaussian_1536_2Gpc_085
hcat=out_z1_nosub_M200b_20p.list
bounds=Utils/genBounds/Output/filename
pdfile=Output/Subsampling/PD/z1-000-
EXT=_PD.gz

start=$(date)
echo "start time : $start"
                                                                                                                                
python main.py  --hfile ${hdir}${hsim}${hdir2}${hcat} --pd True  --pi False  --boundsName ${bounds}${EXT} --arrayjob True --pdfile ${pdfile}${hsim}${EXT} --cut 0 --onedim True












