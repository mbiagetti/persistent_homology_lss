# Type of halo catalog. 1: Rockstar Halos, 2: Minerva halos, 3: Flagship halos
cat = 1
# BOX SIZE OF THE SIMULATIONS IN MPC/H
box = 2000.
# SIZE OF THE SUB-BOXES
smallbox = 1000
# redshift
z = 1
# Minimum Mass of Halos
mmin = 9.189e12
# Maximum Mass of Halos
mmax = 1e20
# Choose method: 0 = alpha filtration, 1 = sublevel filtration, 2 = alpha-DTM filtration, 3 = alpha-DTM with length-DTM mixing
method = 3
# number of halos per subbox
nhalo = 252321
# outlier threshold for calculation of statistics
thresh = 1.3
