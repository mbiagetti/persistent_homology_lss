import argparse

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--hfile', required=True, help = 'Input file for halo catalog')
    parser.add_argument('--pd', default = False, help='Compute Persistence Diagrams')   
    parser.add_argument('--pdfile', required=True, default = None, help='Input/Output file for Persistence Diagram')  
    parser.add_argument('--pi', default = False, help='Compute Persistence Images')  
    parser.add_argument('--npool', default = 2, type = int, help ="Number of processors")
    parser.add_argument('--arrayjob', default = False, help ='Run multiple runs as an array job')  
    parser.add_argument('--onedim', default = False, help ="compute one-dimensional functions")
    parser.add_argument('--cut', default = 0, help ="cut to cycles with death>cut")
    parser.add_argument('--boundsName', default = '0',help = 'name of file with saved bounds for summary statistic computation')

    args = parser.parse_args()

    return args




