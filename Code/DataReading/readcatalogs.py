import pandas as pd
import numpy as np

def readcats(fname,cat):
    if cat == 1:                        #byagox
        vec = np.loadtxt(fname)
        vec = vec[:,:4]    #only take velocities
    if cat == 2:                        #minerva
        dt = np.dtype([('dummy', 'i4'),('Pos', 'f4',3),('Vel', 'f4',3),('Mass', 'f4'),('dummy2', 'i4')])
        mycat = np.fromfile(ifile, dtype=dt)
        vec = np.array([mycat['Mass'][:],mycat['Pos'][:,0], mycat['Pos'][:,1], mycat['Pos'][:,2]]).T
    if cat == 3:                        #flagship
        vec = pd.read_parquet(fname, engine='pyarrow', columns=['halo_lm','x','y','z'])
        vec = np.asarray(vec)
        vec[:,0] = 10**(vec[:,0])
    return vec
