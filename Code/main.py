import numpy as np
import os
import user
import param as par
import fclaux as fc
import DataReading.readcatalogs as readcat
import pdiagram as pd
import pimages as pi



if __name__=="__main__":
    
    args = user.parse_arguments()

    '''PRINT INITIAL LOG'''
    ##############################################################
    print('\n\n******** [PHLSS] - Public Release v1.0 ********\n ')

    if args.hfile is not None:
        print('Halo catalog: %s' %args.hfile)
        print('box size [Mpc/h] = %g\n\n' %par.box)
    ##############################################################

    #---For Array Job---#
    if args.arrayjob:
        runid = os.getenv('SLURM_ARRAY_TASK_ID')
        if args.hfile is not None:
            hfile = args.hfile.replace('run1','run'+str(runid))
        if args.pdfile is not None:
            pdfile = args.pdfile.replace('run1','run'+str(runid))
    else:
        hfile = args.hfile
        pdfile = args.pdfile
    if args.pd:
        if os.path.exists(pdfile) and os.path.getsize(pdfile) > 0:
            print('WARNING: you are going to overwrite an existing PD file!')
        print('Loading halos...\n')
        dat = readcat.readcats(hfile,par.cat)
        print(np.shape(dat))
        #check for user-given limits
        dat = dat[(dat[:,0] > par.mmin) & (dat[:,0] < par.mmax)]  
        print('Loaded %d halos.' %len(dat[:,0]))
        pd.DIAG(dat = dat,  npool = args.npool, outfile = pdfile)  
        #compute bounds for curves if necessary
        if not(os.path.exists(args.boundsName)) or os.path.getsize(pdfile) == 0:
            print('computing bounds file for statistics')
            boundsName=args.boundsName
            if par.method==0: power=0.5
            else: power=1.0
            pd.saveBounds(pdfile, boundsName, power, par.thresh)
    if args.pi:
        print('Reading Persistence Diagram file: %s\n' %pdfile)       
        if os.path.exists(pdfile) and os.path.getsize(pdfile) > 0:        
            pi.IMAG( cut = args.cut, npool = args.npool, outfile=pdfile,  onedim = True, boundsName = args.boundsName)
        else:
            print('Persistence Diagram file not found.')
            raise SystemExit