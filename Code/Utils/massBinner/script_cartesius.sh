#!/bin/sh                                                                       

#!/bin/sh                                                                           

# THE NAME OF THE SBATCH JOB                                                       
#SBATCH -J massbinner

# REDIRECT OUTPUT TO THE FOLLOWING STREAM                                          
#SBATCH -e RUNinfo/massbinner-error%j
#SBATCH  -o RUNinfo/massbinner-out%j
                                         
# NUMBER OF NODES                                                                  
#SBATCH -n 1                                                                   
##SBATCH --mincpus=24

# ENVIRONMENT                                                                       
#SBATCH --partition=short
##SBATCH --constraint=[island1|island2|island3|island4|island5]                    

# SET THE TIME LIMIT [HRS:MINS:SECS]                                               
#SBATCH --time=1:00:00                                                            

#SBATCH --mail-user=m.biagetti@uva.nl                                              
#SBATCH --mail-type=ALL 

module load 2019
module load Python/3.6.6-intel-2018b

######################################
INFILE1=out_z0_nosub_M200b_20p.list
INDIR1=/projects/0/matteob/Nbody/byagox_gaussian_1536_2Gpc_085_run1/FOF/Rockstar/
OUT=output/mbins_3bins_z0.0_byagox_gaussian_1536_2Gpc_085bis.dat
MMAX=3.0e15

python -u mass_binner.py --ifile ${INDIR1}${INFILE1} --mfile $OUT --mmax $MMAX

#INFILE1=out_z1_nosub_M200b_20p.list
#INDIR1=/projects/0/matteob/Nbody/byagox_gaussian_1536_2Gpc_085_run1/FOF/Rockstar/
#OUT=/home/matteob/Codes/py-power/PSpectrum/input/mbins_3bins_z1.0_byagox_gaussian_1536_2Gpc_085.dat
#MMAX=1e15

#python -u mass_binner.py --ifile ${INDIR1}${INFILE1} --mfile $OUT --mmax $MMAX
