import numpy as np
import os
import argparse

#This code provides mass bins intervals from a halo catalogs
#only equal number density mass bins are available for now
#more coming soon

def parse_arguments():
   parser = argparse.ArgumentParser()
   parser.add_argument('--ifile', required=True, help = 'Halo catalog to analyse')
   parser.add_argument('--mfile', required=True, help = 'Output file')
   parser.add_argument('--mmin', default= 9.18903E12, help = 'Minimum halo mass')
   parser.add_argument('--mmax', default= 3E15, help = 'Minimum halo mass')
   parser.add_argument('--nbin', default= 3, type=int, help = 'Number of bins')
   parser.add_argument('--mbin', default= 0, type=int, help = 'Binning procedure. 0: Equal number density')

   args = parser.parse_args()

   return args

def equal_nden( vec = None, nbin = None):
    mass = vec[:,0]
    #sort array
    m_ind= np.size(mass)//nbin
    print('--->Average number of halos per bin= %d' %m_ind)
    mass = np.sort(mass)
    massbin = np.zeros((nbin,2))
    for i in range(1,nbin+1):
        print('Bin number: %d\n '%i)
        mminb = mass[m_ind*(i-1)]
        mmaxb = mass[m_ind*(i)-1]
        nmassb = np.count_nonzero((mass >= mass[m_ind*(i-1)]) & (mass < mass[m_ind*(i)-1]) )
        massbin[i-1,:] = np.array([mminb, mmaxb])
        print('--->Mmin = %.2e' %mminb)
        print('--->Mmax = %.2e\n '%mmaxb)
        print('--->Number of halos in this bin %d\n' %nmassb)
    return massbin.T

if __name__=="__main__":

    args = parse_arguments()

    print('\n*** MASS BINNER ***\n')
    print('Halo catalog: %s' %args.ifile)
    print('Minimum mass: %.2e' %args.mmin)
    print('Maximum mass: %.2e' %args.mmax)
    print('Number of bins: %d' %args.nbin)
    print('Output file: %s\n' %args.mfile)

    #READ THE FULL HALO CATALOG
    print('--->Reading halo catalog.\n')
    vec = np.loadtxt(args.ifile)
    #check for user-given limits
    vec = vec[(vec[:,0] > args.mmin) & (vec[:,0] < args.mmax)]  
    #DETERMINE MASS BINS  
    if args.mbin == 0:
        massbin = equal_nden( vec = vec, nbin = args.nbin)
        np.savetxt(args.mfile, np.array([massbin[:,i] for i in range(args.nbin)]), fmt=('%0.12e','%0.12e'))
