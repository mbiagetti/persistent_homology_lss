#!/bin/sh                                                                       

#First run the halo counter on the whole set you want to subsample

DIR=/users/matteobiagetti/GitProjects/temp/persistent_homology_lss/Code/TestData/
FILE=out_z2_nosub_M200b_20p.list
OUTDIR=output/halo_count_gaussian_1536_2Gpc_085_run

python halo_counter.py --infile ${DIR}${FILE} --outfile ${OUTDIR}_z2p00.dat


#then find the minimum number of halos among all the subboxes

python subBoxer.py