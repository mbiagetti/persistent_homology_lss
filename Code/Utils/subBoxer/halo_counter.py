import numpy as np
import argparse

#This code splits a halo catalog in N sub-boxes and counts halos in them.

def parse_arguments():
   parser = argparse.ArgumentParser()
   parser.add_argument('--infile', required=True, help = 'Halo catalog to analyse')
   parser.add_argument('--outfile', required=True, help = 'Output file')
   parser.add_argument('--mmin', default= 9.18903E12, help = 'Minimum halo mass')
   parser.add_argument('--subbox', default= 2, type=int, help = 'Number of subboxes. Provide n>1 in n^3.')
   parser.add_argument('--box', default= 2000., type=float, help = 'Simulation box side in Mpc/h')

   args = parser.parse_args()

   return args

if __name__=="__main__":

    args = parse_arguments()


    fname = args.infile
    outfile = args.outfile
    print('\n***HALO COUNTER***\n')
    print('---->Loading. FILE: %s\n' %fname)
    dat =np.loadtxt(fname)
    dat=dat[dat[:,0]>args.mmin]
    print('Total number of halos: %d' %dat[:,0].size)
    nsub = args.subbox
    smallbox = args.box/nsub
    print('Slicing the halo catalog into %d sub boxes of size %.2f Mpc/h...\n' %(nsub**3,smallbox))
    xx, yy, zz = np.mgrid[0:nsub, 0:nsub, 0:nsub]
    sample = np.vstack([xx.ravel(),yy.ravel(), zz.ravel()]).T
    num = np.zeros(sample[:,0].size)
    for i in range(sample[:,0].size):
        x0,y0,z0 = sample[i,0],sample[i,1],sample[i,2]
        dat_slice=dat[(dat[:,3]>smallbox*z0)&(dat[:,3]<smallbox*(z0+1))]
        dat_slice=dat_slice[(dat_slice[:,2]>smallbox*y0)&(dat_slice[:,2]<smallbox*(y0+1))]
        dat_slice=dat_slice[(dat_slice[:,1]>smallbox*x0)&(dat_slice[:,1]<smallbox*(x0+1))]
        num[i]=dat_slice[:,0].size
    print('---->Saving. FILE: %s\n' %outfile)
    np.savetxt(outfile,np.c_[sample[:,0], sample[:,1],sample[:,2], num], fmt=('%d'))