import numpy as np
import glob

if __name__=="__main__":

    print('***SUBBOXER***\n\n')
    print('Loading all files in this folder')

    GList=[]
    for elm in np.sort(glob.glob('output/*')):
        #print(elm)
        temp=np.loadtxt(elm,usecols=3,dtype=int)
        GList.append(temp)

    print('Minimum number of halos in subbox is %d' %np.min(GList))
