#!/bin/sh                                                                                      

# THE NAME OF THE SBATCH JOB                                                                   

#SBATCH -J ph                                                                                  

# REDIRECT OUTPUT TO THE FOLLOWING STREAM                                                      
#SBATCH -e RUNinfo/ph-error%j                                                            
#SBATCH -o RUNinfo/ph-out%j                                                              

# NUMBER OF NODES                                                                              
#SBATCH -n 1                                                                                   
##SBATCH --array 1                                                                              
#SBATCH --mincpus=24                                                                           

# ENVIRONMENT                                                                                  

#SBATCH --partition=short                                                                      
##SBATCH --constraint=[island1|island2|island3|island4|island5]                                

# SET THE TIME LIMIT [HRS:MINS:SECS]                                                           
#SBATCH --time=01:00:00                                                                        

#SBATCH --mail-user=m.biagetti@uva.nl                                                                      


module load 2019
module load Python/3.6.6-intel-2018b

#First remove all content in output folder

rm output/*

#Now select which set of catalogs to subsample and how 
#then count halos in each subbox for each catalog

DIR=/projects/0/matteob/Nbody/byagox_gaussian_1536_2Gpc_085_run
FILE=/FOF/Rockstar/out_z1_nosub_M200b_20p.list
OUTDIR=output/halo_count_gaussian_1536_2Gpc_085_run

for i in `seq 1 15`;
do
    echo run$i
    python halo_counter.py --infile ${DIR}${i}${FILE} --outfile ${OUTDIR}${i}_z1p00.dat
done

#then find the minimum number of halos among all the subboxes

python subBoxer.py
