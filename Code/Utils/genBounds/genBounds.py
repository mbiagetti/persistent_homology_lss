import numpy as np
import fclaux as fc
import sys

#this code generates bounds for topological curves from an example PD
#it loads a PD and saves suggested bounds as a numpy array
#python genBounds.py PDname Outfile Power (=0.5 for alpha, 1 for others)
#afterwards you should still check the output file, since outliers can cause funny behavior.
#these cases are easily identified and can be fixed by hand.

#PDname=sys.argv[1]
#outfile=sys.argv[2]
#dimPow=sys.argv[3]

def saveBounds(PDname, outfile, dimPow, thresh):
    diag=np.loadtxt(PDname,delimiter=',')
    diag=[[elm[0],[elm[1],elm[2]]] for elm in diag]
    power=float(dimPow)
    p0,p1,p2=fc.cleanPD(diag,0,power)
    Bounds0=np.concatenate((np.min(p0,axis=0),\
        np.max(p0,axis=0), [np.min(p0[:,0]+p0[:,1])],[np.max(p0[:,0]+p0[:,1])]),axis=0)
    Bounds1=np.concatenate((np.min(p1,axis=0),\
        np.max(p1,axis=0), [np.min(p1[:,0]+p1[:,1])],[np.max(p1[:,0]+p1[:,1])]),axis=0)
    Bounds2=np.concatenate((np.min(p2,axis=0),\
        np.max(p2,axis=0), [np.min(p2[:,0]+p2[:,1])],[np.max(p2[:,0]+p2[:,1])]),axis=0)
    toSave=np.array([Bounds0,Bounds1,Bounds2])
    '''perform check for outliers'''
    toSave[1,:]=np.min([[np.inf,np.inf,thresh*toSave[0,2],thresh*toSave[0,3],np.inf,
        thresh*toSave[0,5]],toSave[1,:]],axis=0)
    toSave[2,:]=np.min([[np.inf,np.inf,thresh*toSave[1,2],thresh*toSave[1,3],np.inf,
        thresh*toSave[1,5]],toSave[2,:]],axis=0)
    
    np.savetxt(outfile,toSave,delimiter=',')
    
    