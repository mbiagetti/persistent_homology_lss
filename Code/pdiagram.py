import numpy as np
import fclaux as fc
import param as par
import gudhi
import time
import tqdm
import parmap
from sklearn.neighbors import NearestNeighbors, KernelDensity, KDTree
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.decomposition import PCA
import multiprocessing
import random


def saveBounds(PDname, outfile, dimPow, thresh):
    diag = np.loadtxt(PDname, delimiter=',')
    diag = [[elm[0], [elm[1], elm[2]]] for elm in diag]
    power = float(dimPow)
    p0, p1, p2 = fc.cleanPD(diag, 0, power)
    Bounds0 = np.concatenate((np.min(p0, axis=0), \
                              np.max(p0, axis=0), [np.min(p0[:, 0] + p0[:, 1])], [np.max(p0[:, 0] + p0[:, 1])]), axis=0)
    Bounds1 = np.concatenate((np.min(p1, axis=0), \
                              np.max(p1, axis=0), [np.min(p1[:, 0] + p1[:, 1])], [np.max(p1[:, 0] + p1[:, 1])]), axis=0)
    Bounds2 = np.concatenate((np.min(p2, axis=0), \
                              np.max(p2, axis=0), [np.min(p2[:, 0] + p2[:, 1])], [np.max(p2[:, 0] + p2[:, 1])]), axis=0)
    toSave = np.array([Bounds0, Bounds1, Bounds2])
    '''perform check for outliers'''
    toSave[1, :] = np.min([[np.inf, np.inf, thresh * toSave[0, 2], thresh * toSave[0, 3], np.inf,
                            thresh * toSave[0, 5]], toSave[1, :]], axis=0)
    toSave[2, :] = np.min([[np.inf, np.inf, thresh * toSave[1, 2], thresh * toSave[1, 3], np.inf,
                            thresh * toSave[1, 5]], toSave[2, :]], axis=0)

    np.savetxt(outfile, toSave, delimiter=',')


class DIAG:

    def __init__(self, dat=None, npool=None, outfile=None):
        self.dat = dat
        self.npool = npool
        self.outfile = outfile
        self.box = par.box
        self.smallbox = par.smallbox
        self.nhalo = par.nhalo
        self.method = par.method
        '''Here do checks on input params'''
        if par.box % par.smallbox != 0:
            print(" Incorrect value for the sub box size. Please choose an integer dividend of the box size.\n")
            raise SystemExit
        self.nsub = int(self.box // self.smallbox)
        print(
            "Slicing the halo catalog into %d sub boxes of side length %d Mpc/h...\n" % (self.nsub ** 3, self.smallbox))
        xx, yy, zz = np.mgrid[0:self.nsub, 0:self.nsub, 0:self.nsub]
        self.xyz_sample = np.vstack([xx.ravel(), yy.ravel(), zz.ravel()]).T
        print("Now compute Persistence Diagrams...\n")
        self.parallelPD()

    def computeSavePDsub(self, sample=None, dat=None, smallbox=None, outfile=None):
        x0, y0, z0 = sample[0], sample[1], sample[2]
        dat_slice = dat[(dat[:, 3] > smallbox * z0) * (dat[:, 3] < smallbox * (z0 + 1))]
        dat_slice = dat_slice[(dat_slice[:, 2] > smallbox * y0) * (dat_slice[:, 2] < smallbox * (y0 + 1))]
        dat_slice = dat_slice[(dat_slice[:, 1] > smallbox * x0) * (dat_slice[:, 1] < smallbox * (x0 + 1))]
        minH = 60000
        ind = np.random.choice(range(np.shape(dat_slice)[0]), minH, replace=False)
        dat_slice = dat_slice[ind, :]
        # print("Done slicing\n")
        nbrs = NearestNeighbors(n_neighbors=15, algorithm='ball_tree').fit(dat_slice[:, 1:])
        xx, yy, zz = np.mgrid[smallbox * x0:smallbox * (x0 + 1):4, smallbox * y0:smallbox * (y0 + 1):4,
                     smallbox * z0:smallbox * (z0 + 1):4]
        xyz_sample = np.vstack([xx.ravel(), yy.ravel(), zz.ravel()]).T
        distances, indices = nbrs.kneighbors(xyz_sample)
        DTM = np.array([np.sqrt(np.sum(
            [((distances[i][j]) / np.log10(dat_slice[indices[i][j]][0])) ** 2 for j in range(len(distances[i]))])) for i
                        in range(len(distances))])
        # DTM=np.reshape(DTM, xx.shape)
        dim = smallbox // 4
        cc = gudhi.CubicalComplex(dimensions=[dim, dim, dim], top_dimensional_cells=DTM)
        diag_cc = cc.persistence(min_persistence=0.01)
        # print('persistent homology computed\n')
        outfile = outfile.replace("000", str(x0) + str(y0) + str(z0))
        outfile = outfile.replace("PD.gz", "PD_sub.gz")
        # print(' Now saving file %s ' %outfile)
        fc.savePD(diag_cc, outfile)

    # adapted from https://github.com/GUDHI/TDA-tutorial/blob/master/Tuto-GUDHI-DTM-filtrations.ipynb
    # see also the companion paper https://arxiv.org/abs/1811.04757
    def Filtration_value(self, p, fx, fy, d, n=10):
        # p can only be 1, 2, or np.inf
        if p == np.inf:
            value = max([fx, fy, d / 2])
        else:
            fmax = max([fx, fy])
            if d < (abs(fx ** p - fy ** p)) ** (1 / p):
                value = fmax
            elif p == 1:
                value = (fx + fy + d) / 2
            elif p == 2:
                value = np.sqrt(((fx + fy) ** 2 + d ** 2) * ((fx - fy) ** 2 + d ** 2)) / (2 * d)
            else:
                Imin = fmax;
                Imax = (d ** p + fmax ** p) ** (1 / p)
                for i in range(n):
                    I = (Imin + Imax) / 2
                    g = (I ** p - fx ** p) ** (1 / p) + (I ** p - fy ** p) ** (1 / p)
                    if g < d:
                        Imin = I
                    else:
                        Imax = I
                value = I
        return value

    # adapted from https://github.com/GUDHI/TDA-tutorial/blob/master/Tuto-GUDHI-DTM-filtrations.ipynb
    def DTM(self, X, query_pts, m):
        N_tot = X.shape[0]
        k = int(np.floor(m * N_tot)) + 1  # number of neighbors

        kdt = KDTree(X, leaf_size=30, metric='euclidean')
        NN_Dist, NN = kdt.query(query_pts, k, return_distance=True)

        DTM_result = np.sqrt(np.sum(NN_Dist * NN_Dist, axis=1) / (k-1))

        return (DTM_result)

    # adapted from https://github.com/GUDHI/TDA-tutorial/blob/master/Tuto-GUDHI-DTM-filtrations.ipynb
    def AlphaWeightedFiltration(self, X, m, lDTMmix=False, filtration_max=np.inf):
        N_tot = X.shape[0]
        p = 2
        ac = gudhi.AlphaComplex(points=X)
        st_alpha = ac.create_simplex_tree()
        # reorder X according to indices of AlphaComplex
        X = np.array([ac.get_point(i) for i in range(len(X))])
        print('alpha complex computed')
        F = self.DTM(X, X, m)

        st = gudhi.SimplexTree()  # create an empty simplex tree

        for simplex in st_alpha.get_filtration():
            if len(simplex[0]) == 1:
                i = simplex[0][0]
                st.insert([i], filtration=F[i])
            if len(simplex[0]) == 2:  # add edges with corresponding filtration value
                i = simplex[0][0]
                j = simplex[0][1]
                if lDTMmix == True:
                    value = self.Filtration_value(p, F[i], F[j], np.linalg.norm(X[i] - X[j]))
                else:
                    value = max([F[i], F[j]])
                st.insert([i, j], filtration=value)
            if len(simplex[0]) == 3:
                i = simplex[0][0]
                j = simplex[0][1]
                k = simplex[0][2]
                value = max([st.filtration([i, j]), st.filtration([j, k]), st.filtration([i, k])])
                st.insert([i, j, k], filtration=value)
            if len(simplex[0]) == 4:
                i = simplex[0][0]
                j = simplex[0][1]
                k = simplex[0][2]
                l = simplex[0][3]
                value = max([st.filtration([i, j, k]), st.filtration([j, k, l]),
                             st.filtration([i, k, l]), st.filtration([i, j, l])])
                st.insert([i, j, k, l], filtration=value)
        return st

    def AlphaDTMFiltration(self, X, m, lDTMmix=True, filtration_max=np.inf):

        # note that due to reordering we compute DTM within AlphaWeightedFiltration
        # DTM_values = self.DTM(X, X, m)
        # print('-->DTM computed')
        st = self.AlphaWeightedFiltration(X, m, lDTMmix, filtration_max)

        return st

    def computeSavePDalphaDTM(self, sample=None, lDTMmix=None, dat=None, smallbox=None, outfile=None):
        x0, y0, z0 = sample[0], sample[1], sample[2]
        dat_slice = dat[(dat[:, 3] > smallbox * z0) * (dat[:, 3] < smallbox * (z0 + 1))]
        dat_slice = dat_slice[(dat_slice[:, 2] > smallbox * y0) * (dat_slice[:, 2] < smallbox * (y0 + 1))]
        dat_slice = dat_slice[(dat_slice[:, 1] > smallbox * x0) * (dat_slice[:, 1] < smallbox * (x0 + 1))]
        minH = int(self.nhalo)
        print('--->Subsampling each box to %d halos each\n' % self.nhalo)
        ind = np.random.choice(range(np.shape(dat_slice)[0]), minH, replace=False)
        dat_slice = dat_slice[ind, :]
        m = 15 / np.shape(dat_slice)[0]
        print('--->Computing AlphaDTMFiltration')
        st = self.AlphaDTMFiltration(dat_slice[:, 1:], m, lDTMmix)
        print('--->Computing Persistence')
        diag_alpha = st.persistence(min_persistence=0.01)
        outfile = outfile.replace("000", str(x0) + str(y0) + str(z0))
        outfile = outfile.replace("PD.gz", "PD_alphaDTM.gz")
        print('Done. Now saving PD diagrams in FILE: %s\n' % outfile)
        fc.savePD(diag_alpha, outfile)

    def computeSavePDalpha(self, sample=None, dat=None, smallbox=None, outfile=None):
        x0, y0, z0 = sample[0], sample[1], sample[2]
        dat_slice = dat[(dat[:, 3] > smallbox * z0) * (dat[:, 3] < smallbox * (z0 + 1))]
        dat_slice = dat_slice[(dat_slice[:, 2] > smallbox * y0) * (dat_slice[:, 2] < smallbox * (y0 + 1))]
        dat_slice = dat_slice[(dat_slice[:, 1] > smallbox * x0) * (dat_slice[:, 1] < smallbox * (x0 + 1))]
        minH = int(self.nhalo)
        print('--->Subsampling each box to %d halos each\n' % self.nhalo)
        ind = np.random.choice(range(np.shape(dat_slice)[0]), minH, replace=False)
        dat_slice = dat_slice[ind, :]
        print('--->Computing AlphaFiltration')
        ac = gudhi.AlphaComplex(points=dat_slice[:, 1:])
        st = ac.create_simplex_tree()
        print('--->Computing Persistence')
        diag_alpha = st.persistence(min_persistence=0.01)
        outfile = outfile.replace("000", str(x0) + str(y0) + str(z0))
        outfile = outfile.replace("PD.gz", "PD_alpha.gz")
        print('Done. Now saving PD diagrams in FILE: %s\n' % outfile)
        fc.savePD(diag_alpha, outfile)

    def parallelPD(self):
        if self.method == 0:
            parmap.map(self.computeSavePDalpha, self.xyz_sample.tolist(), self.dat, self.smallbox,
                       self.outfile, pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
            print("Computed Persistence Diagrams alpha filtration.")
        if self.method == 1:
            parmap.map(self.computeSavePDsub, self.xyz_sample.tolist(), self.dat, self.smallbox,
                       self.outfile, pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
            print("Computed Persistence Diagrams sublevel filtration.")
        if self.method == 2:
            parmap.map(self.computeSavePDalphaDTM, self.xyz_sample.tolist(), False, self.dat, self.smallbox,
                       self.outfile, pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
            print("Computed Persistence Diagrams alpha-DTM filtration.")
        if self.method == 3:
            parmap.map(self.computeSavePDalphaDTM, self.xyz_sample.tolist(), True, self.dat, self.smallbox,
                       self.outfile, pm_processes=self.npool, pm_chunksize=1, pm_pbar=True)
            print("Computed Persistence Diagrams alpha-DTM with length-DTM mixing.")
