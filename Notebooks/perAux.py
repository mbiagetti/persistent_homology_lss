import sys
import numpy as np
import matplotlib.pyplot as plt
import gudhi
import time
from sklearn.neighbors import NearestNeighbors, KernelDensity
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm
from sklearn.linear_model import LogisticRegression
import glob
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
import multiprocessing
import random
import matplotlib.colors as colors
from sklearn.preprocessing import StandardScaler
#from persistence_graphical_tools_Bertrand import *
from sklearn.neighbors import KDTree
from sklearn.metrics.pairwise import euclidean_distances
import math

def PI(sigma,pd,bounds,res=[30,30]):
    #nbrs = NearestNeighbors(n_neighbors=nn).fit(pd)
    kde=KernelDensity(bandwidth=sigma,algorithm='kd_tree',kernel = 'epanechnikov').fit(pd,sample_weight=[elm[1] for elm in pd])
    #kde=KernelDensity(bandwidth=sigma,algorithm='kd_tree',kernel = 'epanechnikov').fit(pd)
    #kde=KernelDensity(bandwidth=sigma,algorithm='kd_tree',kernel = 'epanechnikov').fit(pd)
    x = np.linspace(bounds[0], bounds[1], res[0])
    y = np.linspace(bounds[2], bounds[3], res[1])
    xx, yy = np.meshgrid(x, y)
    xx=xx.ravel()
    yy=yy.ravel()
    xy_sample = np.array([[xx[i],yy[i]] for i in range(len(xx))])
    d=np.exp(kde.score_samples(xy_sample))
    return np.reshape(d,(res[1],res[0]))*sum([elm[1] for elm in pd])

def savePD(d,filename):
    toSave=[]
    for elm in d:
        if elm[1][1]!=elm[1][0]:
            toSave.append([elm[0],elm[1][0],elm[1][1]])
    np.savetxt(filename,toSave,delimiter=',')
    
def cleanPD(pd):
    p0=[]
    p1=[]
    p2=[]
    for elm in pd:
        if elm[0]==2 and elm[1][1]!=elm[1][0]:
            p2.append([elm[1][0],elm[1][1]-elm[1][0]])
        elif elm[0]==1 and elm[1][1]!=elm[1][0]:
            p1.append([elm[1][0],elm[1][1]-elm[1][0]])
        elif (np.isinf(elm[1][1]))==False and elm[1][1]!=elm[1][0]:
            p0.append(elm[1][1]-elm[1][0])
    return np.array(p0),np.array(p1),np.array(p2)

def cleanPDsub(pd):
    p0=[]
    p1=[]
    p2=[]
    for elm in pd:
        if elm[0]==2 and elm[1][1]!=elm[1][0]:
            p2.append([elm[1][0],elm[1][1]-elm[1][0]])
        elif elm[0]==1 and elm[1][1]!=elm[1][0]:
            p1.append([elm[1][0],elm[1][1]-elm[1][0]])
        elif (np.isinf(elm[1][1]))==False and elm[1][1]!=elm[1][0]:
            p0.append([elm[1][0],elm[1][1]-elm[1][0]])
    return np.array(p0),np.array(p1),np.array(p2)

def DTM(X,query_pts,m):
    '''
    Compute the values of the DTM (with exponent p=2) of the empirical measure of a point cloud X
    Require sklearn.neighbors.KDTree to search nearest neighbors
    
    Input:
    X: a nxd numpy array representing n points in R^d
    query_pts:  a kxd numpy array of query points
    m: parameter of the DTM in [0,1)
    
    Output: 
    DTM_result: a kx1 numpy array contaning the DTM of the 
    query points
    
    Example:
    X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
    Q = np.array([[0,0],[5,5]])
    DTM_values = DTM(X, Q, 0.3)
    '''
    N_tot = X.shape[0]     
    k = math.floor(m*N_tot)+1   # number of neighbors

    kdt = KDTree(X, leaf_size=30, metric='euclidean')
    NN_Dist, NN = kdt.query(query_pts, k, return_distance=True)  

    DTM_result = np.sqrt(np.sum(NN_Dist*NN_Dist,axis=1) / k)
    
    return(DTM_result)


def Filtration_value(p, fx, fy, d, n = 10):
    '''
    Compute the filtrations values of the edge [x,y] in the weighted Rips filtration
    If p is not 1, 2 or 'np.inf, an implicit equation is solved
    The equation to solve is G(I) = d, where G(I) = (I**p-fx**p)**(1/p)+(I**p-fy**p)**(1/p)
    We use a dichotomic method
    
    Input:
    p: parameter of the weighted Rips filtration, in [1, +inf) or np.inf
    fx: filtration value of the point x
    fy: filtration value of the point y
    d: distance between the points x and y
    n: number of iterations of the dichotomic method
        
    Output: 
    val : filtration value of the edge [x,y], i.e. solution of G(I) = d    
    
    Example:
    Filtration_value(2.4, 2, 3, 5, 10)
    '''
    if p==np.inf:
        value = max([fx,fy,d/2])
    else:
        fmax = max([fx,fy])
        if d < (abs(fx**p-fy**p))**(1/p):
            value = fmax
        elif p==1:
            value = (fx+fy+d)/2
        elif p==2:
            value = np.sqrt( ( (fx+fy)**2 +d**2 )*( (fx-fy)**2 +d**2 ) )/(2*d)            
        else:
            Imin = fmax; Imax = (d**p+fmax**p)**(1/p)
            for i in range(n):
                I = (Imin+Imax)/2
                g = (I**p-fx**p)**(1/p)+(I**p-fy**p)**(1/p)
                if g<d:
                    Imin=I
                else:
                    Imax=I
            value = I
    return value
def AlphaWeightedFiltration0(X, F, p,dimension_max =2, filtration_max = np.inf):
    N_tot = X.shape[0]     
    #distances = euclidean_distances(X)          #compute the pairwise distances #AC: TERRIBLE!

    st_alpha = gudhi.AlphaComplex(points=X).create_simplex_tree()  
    print('alpha complex computed')
    
    st = gudhi.SimplexTree()#create an empty simplex tree
    
    for simplex in st_alpha.get_filtration():            #add vertices with corresponding filtration value
        if len(simplex[0])==1:
            i = simplex[0][0]
            #st_alpha.assign_filtration([i], filtration  = F[i])
            st.insert([i],filtration = F[i])
        if len(simplex[0])==2:                     #add edges with corresponding filtration value
            i = simplex[0][0]
            j = simplex[0][1]
            #value = Filtration_value(p, F[i], F[j], distances[i][j]) #AC: TERRIBLE!!
            #value=Filtration_value(p,F[i],F[j],euclidean_distances([X[i]],[X[j]]))
            value=max([F[i],F[j]])
            #st_alpha.assign_filtration([i,j], filtration  = value)
            st.insert([i,j],filtration=value)
        if len(simplex[0])==3:
            i = simplex[0][0]
            j = simplex[0][1]
            k = simplex[0][2]
            value=max([st.filtration([i,j]),st.filtration([j,k]),st.filtration([i,k])])
            #st_alpha.assign_filtration([i,j,k], filtration  = value)
            st.insert([i,j,k],filtration=value)
        if len(simplex[0])==4:
            i = simplex[0][0]
            j = simplex[0][1]
            k = simplex[0][2]
            l = simplex[0][3]
            value=max([st.filtration([i,j,k]),st.filtration([j,k,l]),
                       st.filtration([i,k,l]),st.filtration([i,j,l])])
            #st_alpha.assign_filtration([i,j,k,l], filtration  = value)
            st.insert([i,j,k,l],filtration=value)
            
    print('weighted complex computed')
    #st.expansion(dimension_max)                 #expand the complex
    #st_alpha.make_filtration_non_decreasing()
    result_str = 'Alpha Weighted Complex is of dimension ' + repr(st.dimension()) + ' - ' + \
        repr(st.num_simplices()) + ' simplices - ' + \
        repr(st.num_vertices()) + ' vertices.' +\
        ' Filtration maximal value is ' + str(filtration_max) + '.'
    print(result_str)

    return st




def AlphaDTMFiltration0(X, m, p, dimension_max =2, filtration_max = np.inf):
    
    DTM_values = DTM(X,X,m)
    print('DTM computed')
    st = AlphaWeightedFiltration0(X, DTM_values, p, dimension_max, filtration_max)

    return st
