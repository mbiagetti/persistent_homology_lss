![](images/eos.png)

# The Persistence of Large Scale Structure

We implement persistent homology on a point cloud composed of halos positions in a cubic box from N-body simulations of the universe at large scales. The output of the code are persistence diagrams and images that are used to constrain cosmological parameters from the halo catalog. 

All details of the implementation and analysis pipeline are included in:

The Persistence of Large Scale Structure I: Primordial non-Gaussianity [https://arxiv.org/abs/2009.04819]

Authors: Matteo Biagetti and Alex Cole


## Installation and Running

The code is written in Python 3. It requires the following python packages: 

- `numpy`
- `tdqm`
- `parmap`
- `sklearn`
- `gudhi`

All packages can be installed via `pip install package`. In order for the code to run, it needs 

- The path to the halo catalog to analyze 
- A file name for/path to persistence diagrams. If the persistence diagrams exist already, it will be treated as a path to the file and the code will directly calculate persistence images. If they do not exist yet, the code will generate them and save them with that file name.
- A "bounds" file can be generated via the utility code in `Utils/genBounds/`. This is an optional check on the persistence images output.

All other pre-running options can be found in the file `user.py`. Persistence diagrams and images are calculated by turning on the options `--pd` and `--pi`, respectively. It is also possible to cut features at small scales by turning on the `--cut` option.

A compressed halo catalog for testing is available in the `TestData` folder. A simple run of the code looks like:

> python main.py  --hfile TestData/out_z2_nosub_M200b_20p.list --pd True --pi True   --pdfile Output/Subsampling/PD/test_alphaDTM_

A more detailed example script is available in the `Scripts` folder, named `python_script.sh`. A similar script adapted to run on a supercomputer
using the SLURM environment is called `python_script_cartesius.sh`.

## N-body simulations

Full details on the N-body simulations is available at [https://mbiagetti.gitlab.io/cosmos/nbody/eos/]




